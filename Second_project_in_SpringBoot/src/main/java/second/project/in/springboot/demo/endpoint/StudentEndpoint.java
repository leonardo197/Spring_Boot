package second.project.in.springboot.demo.endpoint;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import second.project.in.springboot.demo.error.CustomErrorType;
import second.project.in.springboot.demo.model.Student;
import second.project.in.springboot.demo.repository.StudentRepository;
import second.project.in.springboot.demo.util.DateUtil;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@RequestMapping ("students")
public class StudentEndpoint {
    private final StudentRepository studentDAO;

    @Autowired
    public StudentEndpoint(StudentRepository studentDAO) {
        this.studentDAO = studentDAO;
    }

    @GetMapping
    public ResponseEntity<?> studentList() {
        return new ResponseEntity<>(studentDAO.findAll(), HttpStatus.OK);
    }

    @GetMapping (path = "/{id}")
    public ResponseEntity<?> studentListById(@PathVariable ("id") long id) {
        Student student = studentDAO.findById(id);
        if (student != null) {
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomErrorType("student not found"), HttpStatus.OK);
        }
    }

    @GetMapping (path = "findStudentsByName/{name}")
    public ResponseEntity<?> findStudentsByName(@PathVariable String name) {
        List<Student> student = studentDAO.findByName(name);
        if (student != null) {
            return new ResponseEntity<>(student, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomErrorType("student not found"), HttpStatus.OK);
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(studentDAO);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Student student) {
        if (studentDAO.findById(student.getId()) != null) {
            return new ResponseEntity<>(new CustomErrorType("student already exists"), HttpStatus.NOT_EXTENDED);
        } else {
            return new ResponseEntity<>(studentDAO.save(student), HttpStatus.OK);
        }
    }

    @DeleteMapping (path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        if (studentDAO.findById(id) != null) {
            studentDAO.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new CustomErrorType("student does not exist"), HttpStatus.NOT_EXTENDED);
        }
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody Student student) {
        studentDAO.save(student);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping (method = GET, path = "/Time")
    public String date() {
        DateUtil dateUtil;
        dateUtil = new DateUtil();
        LocalDateTime date = LocalDateTime.now();
        return dateUtil.formatLocalDataTimeToDatabaseStyle(date);
    }
}


package second.project.in.springboot.demo.repository;

import org.springframework.data.repository.CrudRepository;
import second.project.in.springboot.demo.model.Student;

import java.util.List;

public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findByName(String name);

    List<Student> findByAge(int age);

    Student findById(long id);



}

package first.project.endpoint;

import first.project.model.Student;
import first.project.util.DateUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping("student")
public class StudentEndpoint {
    @RequestMapping(method = RequestMethod.GET, path = "/list")
    public List<Student> studentList() {
        return asList(new Student("leo"), new Student("ze"));
    }

    @RequestMapping(method = RequestMethod.GET, path = "/Time")
    public String date() {
        DateUtil dateUtil = new DateUtil();
        LocalDateTime date = LocalDateTime.now();
        return dateUtil.formatLocalDataTimeToDatabaseStyle(date) + " ola";
    }
}

